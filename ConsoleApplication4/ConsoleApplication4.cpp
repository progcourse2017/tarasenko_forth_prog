#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	int size;
	int decimalNum = 0;
	cout << "Введите количество цифр двоичного кода" << endl;
	cin >> size;
	int *array = new int[size];
	for (int i = 0; i < size; i++) 
	{
		cout << "Ввод цифры" << endl;
		cin >> array[i];
	}	
	for (int i = size-1; i>=0; i--) 
	{
		if ((array[i] ==1) || (array[i] == 0))
		{
			if (array[i] == 1)
			{
				decimalNum = decimalNum+pow(2, size - 1 - i);
			}
		}
		else
			cout << "Ошибка при вводе" << endl;
	}
	cout << "Десятичный код " << decimalNum << endl;
	system("pause");
	delete[] array;
    return 0;
}

